#! /usr/bin/env node
/**
 *  Name:  cpclaim.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

const { program } = require("commander");

program
  .version("1.0.0")
  .command("registrar", "Claim registration commands")
  .command(
    "registrar-check <claimkey>",
    "Checks the claim status for one or more devices."
  )
  .command("registrar-claim <acct> <claimkey>", "Claims an unclaimed device")
  .command(
    "registrar-list <acct>",
    "Lists claimed devices for the specified account"
  )
  .command(
    "registrar-info <acct> <claimkey>",
    "Retrieves device information given account and a serial number or claimkey"
  )
  .command(
    "registrar-move <acct> <claimkey> <acctTarget>",
    "Moves a claim to the specified target account."
  )
  .command(
    "registrar-purge <acct> <claimkey>",
    "Removes a claim for the specified device."
  )
  .parse(process.argv);
