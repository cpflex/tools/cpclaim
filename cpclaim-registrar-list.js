#! /usr/bin/env node
/**
 *  Name:  cpclaim-registrar-list.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require("request");
var acctools = require("./tools");
var standard_input = process.stdin;
var sprintf = require("sprintf-js").sprintf;
const util = require("util");
const createCsvWriter = require("csv-writer").createObjectCsvWriter;

const { program } = require("commander");
const tools = require("./tools");
const { publishEnd } = require("./tools");

require("console.table");

var _acct = null;

try {
  program
    .option("-v  --verbose", "provides verbose output.", false)
    .option(
      "-l, --limit <limit>",
      "Specifies the maximum number of records to return.",
      "-1"
    )
    .option(
      "-p, --page <size>",
      "Specifies the page size.  Default is not paged.",
      "-1"
    )
    .option(
      "-a, --autopage",
      "If specified, list is auto-paged, user does not have to press enter between pages.",
      "-1"
    )
    .option(
      "-c, --convert",
      "Convert identity array to a json object map.",
      false
    )
    .description("Lists the claimed devices for an account", {
      acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",
    })
    .arguments("<acct>")
    .action(function (acct) {
      /****************************
       * Main Program
       ***************************/
      _acct = acct;

      program.limit = Number(program.limit);
      program.page = Number(program.page);

      let paging = {
        limit: program.limit,
        pageSize: program.page,
        pageIndex: -1,
        state: [],
      };

      acctools.publishBegin(program);

      GetRegistrarPage(acct, program.verbose, paging, ResponseHandler);
    });

  acctools.addCommonOptions(program);
  program.parse(process.argv);
  acctools.logHeader(program);

  function GetRegistrarPage(acct, verbose, paging, handler) {
    var input = {
      acct: acct,
      verbose,
      verbose,
      paging: paging,
    };

    var req = acctools.postApiRequest(program, "/registrar/list", input);
    request(req, (error, response, body) => {
      var resp = acctools.parseResponseBody(error, response, body);
      if (resp.paging == null || resp.paging.hasMorePages == null) {
        resp.paging.hasMorePages = false;
      }
      handler(resp.paging, resp.claims);
    });
  }

  function ResponseHandler(paging, items) {
    //Convert identities to object map if specified.
    if (program.convert === true) {
      items.forEach((item) => {
        if (item.identities != null) {
          let identities = {};
          item.identities.map((entry) => (identities[entry.id] = entry.value));
          item.identities = identities;
        }
      });
    } else if (program.table || program.csv) {
      //Add identities into main item as properties.
      items.forEach((item) => {
        if (item.identities != null) {
          item.identities.map((entry) => (item[entry.id] = entry.value));
          delete item.identities;
        }

        //Flatten Attributes.
        if (item.attributes != null) {
          Object.assign(item, item.attributes);
          delete item.attributes;
        }
      });
    }

    acctools.publishOutput(program, items, paging.pageIndex);

    if (paging.morePages) {
      //console.log(sprintf("\r\npagingIndex=%d", paging.pageIndex + 1));
      if (!program.autopage) {
        console.log("Next Page <Enter>");
        standard_input.on("data", function (data) {
          GetRegistrarPage(_acct, program.verbose, paging, ResponseHandler);
        });
      } else {
        GetRegistrarPage(_acct, program.verbose, paging, ResponseHandler);
      }
    } else {
      publishEnd(program);
      process.exit(0);
    }
  }
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
