#! /usr/bin/env node
/**
 *  Name:  cpclaim-registrar-claim.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require("request");
const util = require("util");
var acctools = require("./benttools");
const { program } = require("commander");
require("console.table");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------

  program
    .description(
      "Claims and associates the devices with the specified account.",
      {
        acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",
        claimkeys:
          "A claimkey in the format: `XXXX-XXXXXX-XXXX-XXXXXXXXXXXXXXXXXX`.",
      }
    )
    .arguments("[acct] [claimkeys...]")
    .option(
      "-s,  --scope <visibility>",
      "Visibility scope of claim: either public, protected, or private.",
      "protected"
    )
    .option(
      "-k, --keyfile <filename .js | .yaml>",
      "Optional key file with format {'acct':<acct>, claimkeys: [ <claimkey>,...] }, Supports YAML equivalent as well."
    )
    //------------------------------------------------------------------------
    // Command Implementation
    //------------------------------------------------------------------------
    .action(async function (acct, claimkeys) {
      if (program.keyfile) {
        var keyfile = acctools.loadSpecification(program.keyfile);
        if (keyfile.acct) acct = keyfile.acct;
        if (keyfile.claimkeys) {
          claimkeys = keyfile.claimkeys;
        }
      }

      if (!acct) {
        throw new Error("account not specified");
      }

      if (!claimkeys || claimkeys.length == 0) {
        throw new Error("claimkeys not specified");
      }

      var ct = 0;
      console.info("Claiming: ");
      let { post, hdr } = acctools.createPostClient(program, false, 200);
      for (const claimkey of claimkeys) {
        ct++;
        console.info(`${ct}. ${claimkey}`);
        var input = {
          claimkey: claimkey,
          acct: acct,
          visbility: program.scope,
        };

        await post("/registrar/claim", input, hdr);
      }
    });

  acctools.addCommonOptions(program);
  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
