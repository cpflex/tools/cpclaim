#! /usr/bin/env node
/**
 *  Name:  cpclaim-registrar-move.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require("request");
const util = require("util");
var acctools = require("./tools");
const { program } = require("commander");
const { brotliDecompressSync } = require("zlib");

require("console.table");
try {
  program
    .description(
      "**REQUIRES ACCESS KEY**\r\nMoves a claim to the specified target account. Supports key file with format {'acct':<acct>, 'acctTarget': <acctTarget>, claimkeys: [ <claimkey>,...] }, Supports YAML equivalent as well.",
      {
        acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",
        acctTarget:
          "The target account identifier. Can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",
        claimkeys:
          "One or more claimkeys in the format: `XXXX-XXXXXX-XXXX-XXXXXXXXXXXXXXXXXX`.",
      }
    )
    .arguments("[acct] [acctTarget] [claimkeys...] ")
    .option(
      "-k --keyfile <filename .js | .yaml>",
      "Optional claimkey file in JSON or YAML format. If specified arguments are not required."
    )

    //------------------------------------------------------------------------
    // Command Implementation
    //------------------------------------------------------------------------
    .action(function (acct, acctTarget, claimkeys) {
      const body = {
        acct: acct,
        acctTarget: acctTarget,
        claimkeys: claimkeys,
      };

      //Load spec if defined.
      if (program.keyfile) {
        const spec = acctools.loadSpecification(program.keyfile);
        if (!body.acct) {
          body.acct = spec.acct;
        }
        if (!body.acctTarget) {
          body.acctTarget = spec.acctTarget;
        }
        if (spec.claimkeys) {
          body.claimkeys = spec.claimkeys;
        }
      }

      //Validate arguments.
      if (!body.acct || !body.acctTarget || !body.claimkeys.length > 0) {
        console.log("Body: \r\n" + JSON.stringify(body, null, 2));
        throw new Error(
          "Missing required arguments. Check command documentation"
        );
      }

      //Execute the move operation.
      var req = acctools.postApiRequest(program, "/registrar/move", body, true);
      request(req, (error, response, body) => {
        var resp = acctools.parseResponseBody(error, response, body);
        acctools.publishOutput(program, resp);
      });
    });

  acctools.addCommonOptions(program, true);
  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
