#! /usr/bin/env node
/**
 *  Name:  cpclaim-registrar-purge.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require("request");
const util = require("util");
var acctools = require("./tools");
const { program } = require("commander");

require("console.table");
try {
  program
    .description(
      "**REQUIRES ACCESS KEY**\r\nRemoves a claim for the specified device.  This is not recoverable and is permanent.",
      {
        acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",
        claimkey:
          "A claimkey in the format: `XXXX-XXXXXX-XXXX-XXXXXXXXXXXXXXXXXX`.",
      }
    )
    .option(
      "-a, --allclaims",
      "Purges all claims in the account. Use with great care."
    )
    .arguments("<acct> [claimkey]")

    //------------------------------------------------------------------------
    // Command Implementation
    //------------------------------------------------------------------------
    .action(function (acct, claimkey) {
      const body = {
        acct: acct,
        claimkey: claimkey,
      };

      //Support one claim or all claims for an account.
      if (claimkey) {
        body.claimkey = claimkey;
      } else if (program.allclaims) {
        body.allclaims = true;
      } else {
        throw new Error("claimkey or allclaims option must be defined");
      }

      var req = acctools.postApiRequest(
        program,
        "/registrar/purge",
        body,
        true
      );

      request(req, (error, response, body) => {
        var resp = acctools.parseResponseBody(error, response, body);
        acctools.publishOutput(program, resp);
      });
    });

  acctools.addCommonOptions(program);
  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
