#! /usr/bin/env node
/**
 *  Name:  cpclaim-registrar-check.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require("request");
const util = require("util");
//var sprintf = require('sprintf-js').sprintf;
var acctools = require("./tools");
const { program } = require("commander");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------

  program
    .description(
      "Queries the database for the claim status of the device specified by the claim key(s).\r\n" +
        "It also returns the device Product ID, which can help understand the device without actually claiming it. This works for any valid API key.\r\n",
      {
        claimkeys:
          "One ore more device claimkeys in the format: XXXX-XXXXXX-XXXX-XXXXXXXXXXXXXXXXXX.",
      }
    )
    .arguments("<claimkeys...>")

    //------------------------------------------------------------------------
    // Command Implementation
    //------------------------------------------------------------------------
    .action(function (claimkeys) {
      var req = acctools.postApiRequest(program, "/registrar/check", {
        claimkeys: claimkeys,
      });

      request(req, (error, response, body) => {
        var resp = acctools.parseResponseBody(error, response, body);
        acctools.publishOutput(program, resp);
      });
    });

  acctools.addCommonOptions(program);
  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
