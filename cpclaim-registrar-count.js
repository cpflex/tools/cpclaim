#! /usr/bin/env node
/**
 *  Name:  cpclaim-registrar-count.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require("request");
const util = require("util");
//var sprintf = require('sprintf-js').sprintf;
var acctools = require("./tools");
const { program } = require("commander");

require("console.table");

try {
  program
    .description("Gets the count of claimed devices in an account.", {
      acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",
    })
    .arguments("<acct>")
    .action(function (acct) {
      var input = {
        acct: acct,
      };

      var req = acctools.postApiRequest(
        program,
        "/registrar/count",
        input,
        false
      );

      request(req, (error, response, body) => {
        var resp = acctools.parseResponseBody(error, response, body);
        console.log(`Count claims: ${resp.count}`);
      });
    });
  acctools.addCommonOptions(program);
  program.parse(process.argv);

  acctools.logHeader(program);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
