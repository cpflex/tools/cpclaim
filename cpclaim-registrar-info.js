#! /usr/bin/env node
/**
 *  Name:  cpclaim-registrar-info.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require("request");
const util = require("util");
//var sprintf = require('sprintf-js').sprintf;
var acctools = require("./tools");
const { program } = require("commander");

require("console.table");

program
  .description(
    "Retrieves claimed device information given serial number or claim key along with specified account.",
    {
      acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",
      key: "Devices are identified by either a Serial Number in the following format: `XXXX-XXXXXX-XXXX` or a claimkey in the format: `XXXX-XXXXXX-XXXX-XXXXXXXXXXXXXXXXXX`.",
    }
  )
  .arguments("<acct> <key>")
  .option(
    "-c, --convert",
    "Convert identity array to a json object map.",
    false
  )
  .action(function (acct, key) {
    _acct = acct;
    _key = key;
  });
acctools.addCommonOptions(program);
program.parse(process.argv);

acctools.logHeader(program);
console.log("");

var _table = [];

/**
 * Function retrieves the claim info
 * @param {*} nodeuri
 */
function GetClaimInfo(acct, key, handler) {
  var input = {
    key: key,
    acct: acct,
  };

  var req = acctools.postApiRequest(program, "/registrar/info", input, false);

  request(req, (error, response, body) => {
    var resp = acctools.parseResponseBody(error, response, body);
    handler(resp);
  });
}

GetClaimInfo(_acct, _key, function (resp) {
  //Convert identities to object map if specified.
  if (program.convert === true && resp.identities != null) {
    let identities = {};
    resp.identities.map((entry) => (identities[entry.id] = entry.value));
    resp.identities = identities;
  }

  if (program.json === true) console.log(JSON.stringify(resp, null, 2));
  else console.log(util.inspect(resp, false, null, true));
});
