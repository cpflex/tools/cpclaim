#! /usr/bin/env node
/**
 *  Name:  cpclaim.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

const { program } = require("commander");

program
  .version("1.0.0")
  .command("check <claimkey...>", "Checks the status of one or more claimkeys")
  .command("claim <acct> <claimkey>", "Claims an unclaimed device")
  .command("list <acct>", "Lists claimed devices for the specified account")
  .command(
    "info <acct> <claimkey>",
    "Retrieves device information given account and a serial number or claimkey"
  )
  .command(
    "move <acct> <claimkey> <acctTarget>",
    "Moves a claim to the specified target account."
  )

  .command(
    "purge <acct> <claimkey>",
    "Removes a claim for the specified device."
  )
  .parse(process.argv);
